# Next.js Boilerplate Repo

[![pipeline status](https://gitlab.com/antoniusanggito/ppl-frontend/badges/main/pipeline.svg)](https://gitlab.com/antoniusanggito/ppl-frontend/-/commits/main)
[![coverage report](https://gitlab.com/antoniusanggito/ppl-frontend/badges/main/coverage.svg)](https://gitlab.com/antoniusanggito/ppl-frontend/-/commits/main)

Pake versi node terbaru (v16), bisa langsung `npm install`, lalu untuk development `npm run dev`.

Stack: Next.js (React framework), Typescript (Javascript++), Twin.macro (Tailwind++ untuk styling)

Yang utama isinya folder `pages` sama `components` aja.

Halo

TODO:

- [x] Tes deploy PWA: https://ppl-frontend.vercel.app/
- [x] Tes request API: pake fetch + react-query
- [ ] Setup global styling
- [x] Fix performance lighthouse masih 70: fix di laptop mario
